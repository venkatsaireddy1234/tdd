const chai = require("chai");
const expect = chai.expect;

describe("find out a list or string is a palindrome or not", () => {
  context("where the list or string is empty", () => {
    it("should return nothing", () => {
      let result = findPalindromeOrNot([]);
      expect(result).to.be.false;
    });
  });
  context("where no list or string is passed", () => {
    it("should return undefined", () => {
      let result = findPalindromeOrNot();
      expect(result).to.be.false;
    });
  });

  context("when there is only one element in the list or string", () => {
    it("should return undefined", () => {
      let result = findPalindromeOrNot([1]);
      expect(result).to.be.true;
    });
  });
  context("when there are multiple elements in the string or list", () => {
    it("should return whether palindrome or not", () => {
      let result = findPalindromeOrNot("madamimadam");
      expect(result).to.be.true;
    });
  });
});

const findPalindromeOrNot = (list = []) => {
  if (list.length === 0) {
    return false;
  }
  if (typeof list === "string") {
    return list === list.split("").reverse().join("");
  } else {
    return JSON.stringify(list) === JSON.stringify(list.reverse());
  }
};
