const chai = require("chai");
const expect = chai.expect;

describe("Eliminate consecutive duplicates of list ", () => {
  context("When the list is not passed", () => {
    it("Should return nothing", () => {
      const result = EliminateConsecutiveDublicates();
      expect(result).to.be.undefined;
    });
  });
  context("When the list is empty", () => {
    it("Should return nothing", () => {
      const result = EliminateConsecutiveDublicates([]);
      expect(result).to.be.undefined;
    });
  });
  context("When the list contains one element", () => {
    it("Should return that element", () => {
      const result = EliminateConsecutiveDublicates(["a"]);
      expect(result).to.be.eql(["a"]);
    });
  });
  context("When the list contains more than one element", () => {
    it("Should return list of those elements consecutively without duplicates", () => {
      const result = EliminateConsecutiveDublicates(["a", "a", "a", "b", "b"]);
      expect(result).to.be.eql(["a", "b"]);
    });
  });
  context("When the list contains more than one element", () => {
    it("Should return the string of those elements consecutively without duplicates ", () => {
      const result = EliminateConsecutiveDublicates("aaaabccaadeeee");
      expect(result).to.be.eql("abcade");
    });
  });
});

const EliminateConsecutiveDublicates = (list = []) => {
  if (list.length === 0) {
    return undefined;
  }
  let firstValue = 0,
    secondValue = 1,
    finalValue = "";
  while (secondValue < list.length) {
    if (list[firstValue] !== list[secondValue]) {
      finalValue += list[firstValue];
      firstValue = secondValue;
    }
    secondValue += 1;
  }
  finalValue += list[firstValue];
  return typeof list != "string" ? finalValue.split("") : finalValue;
};
