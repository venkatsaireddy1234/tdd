const chai = require("chai");
const expect = chai.expect;

describe("RunLength encoding of a list of consecutive duplicates into sublists", () => {
  context("When the list is not passed", () => {
    it("should return nothing", () => {
      const result = runLengthEncoding();
      expect(result).to.be.undefined;
    });
  });
  context("When the list is empty", () => {
    it("should return nothing", () => {
      const result = runLengthEncoding([]);
      expect(result).to.be.undefined;
    });
  });
  context("When the list contains only one element", () => {
    it("should return encoded result of that array", () => {
      const result = runLengthEncoding(["a"]);
      expect(result).to.be.eql([[1, "a"]]);
    });
  });
  context("When the list contains two elements", () => {
    it("should return encoded result of that array", () => {
      const result = runLengthEncoding(["a", "b"]);
      expect(result).to.be.eql([
        [1, "a"],
        [1, "b"],
      ]);
    });
  });
  context("When the list contains multiple elements", () => {
    it("should return encoded result of that array", () => {
      const result = runLengthEncoding(["a","b", "c"]);
      expect(result).to.be.eql([
        [1, "a"],
        [1, "b"],
        [1, "c"],
      ]);
    });
  });
});

const runLengthEncoding = (list = []) => {
  if (list.length === 0) {
    return undefined;
  }
  let starting_index = 1,
    counter = 1,
    finalArray = [],
    subList = [];
  while (starting_index < list.length) {
    if (list[starting_index] === list[starting_index - 1]) {
      counter += 1;
    } else {
      subList = [];
      subList.push(counter);
      subList.push(list[starting_index - 1]);
      finalArray.push(subList);
      counter = 1;
    }
    starting_index += 1;
  }
  subList = [];
  subList.push(counter);
  subList.push(list[starting_index - 1]);
  finalArray.push(subList);
  return finalArray;
};
