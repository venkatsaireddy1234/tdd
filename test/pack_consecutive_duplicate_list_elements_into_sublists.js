const chai = require("chai");
const expect = chai.expect;

describe("Pack the consecutive duplicates of list elements into sublists", () => {
  context("When the list is not passed", () => {
    it("Should return nothing", () => {
      const result = packConsecutiveDuplicates();
      expect(result).to.be.undefined;
    });
  });
  context("When the list is empty", () => {
    it("Should return nothing", () => {
      const result = packConsecutiveDuplicates([]);
      expect(result).to.be.undefined;
    });
  });
  context("When the list contains only one element", () => {
    it("Should return that element", () => {
      const result = packConsecutiveDuplicates(["a"]);
      expect(result).to.be.eql(["a"]);
    });
  });
  context("When the list contains two elements", () => {
    it("Should return those elements", () => {
      const result = packConsecutiveDuplicates(["a", "b"]);
      expect(result).to.be.eql(["a", "b"]);
    });
  });
  context("When the list contains multiple elements", () => {
    it("Should return the pack of consecutives duplicates into sublists", () => {
      const result = packConsecutiveDuplicates([
        "a",
        "a",
        "a",
        "a",
        "b",
        "c",
        "c",
        "a",
        "a",
        "d",
        "e",
        "e",
        "e",
        "e",
      ]);
      expect(result).to.be.eql(["aaaa", "b", "cc", "aa", "d", "eeee"]);
    });
  });
});
const packConsecutiveDuplicates = (list = []) => {
  if (list.length === 0) {
    return undefined;
  }
  let starting_index = 1,
    finalArray = [],
    result = list[0],
    index = 0;
  while (starting_index < list.length) {
    if (list[starting_index] === list[starting_index - 1]) {
      result += list[starting_index];
      starting_index++;
    } else {
      finalArray.push(result);
      result = "";
      result += list[starting_index];
      starting_index++;
    }
  }
  finalArray.push(result);
  return finalArray;
};
