const chai = require("chai");
const expect = chai.expect;

describe("find kth element in a list/string", () => {
  context("when the list/string is empty", () => {
    it("should return nothing", () => {
      let result = findKthElement("");
      expect(result).to.be.undefined;
    });
  });

  context("when there is one element in the list/string and k is equal to one", () => {
    it("should return element of that list/string", () => {
      let result = findKthElement("h", 1);
      expect(result).to.be.eql("h");
    });
  });

  context(
    "when there is multiple elements and k is greater than length of the list/string",
    () => {
      it("should return undefined", () => {
        let result = findKthElement("hai", 4);
        expect(result).to.be.undefined;
      });
    }
  );
  context(
    "when there are multiple elements in the list/string and k is equal to or less then the length of the list/string",
    () => {
      it("should return k-1 index of the list/string", () => {
        let result = findKthElement("hai", 3);
        expect(result).to.be.eql("i");
      });
    }
  );
  
  
});

const findKthElement = (string, k) => {
  return string.length && k <= string.length ? string[k - 1] : undefined;
}

