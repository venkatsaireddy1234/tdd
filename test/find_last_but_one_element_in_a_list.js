const chai = require("chai");
const expect = chai.expect;

describe("find last but one element in a list", () => {
  context("when the list is empty", () => {
    it("should return nothing", () => {
      let result = findLastButOneElement([]);
      expect(result).to.be.undefined;
    });
  });

  context("when the list contains one element", () => {
    it("should return nothing", () => {
      let result = findLastButOneElement([100]);
      expect(result).to.be.undefined;
    });
  });

  context("when the list contains two elements", () => {
    it("it should return first element", () => {
      let result = findLastButOneElement([100, 101]);
      expect(result).to.be.eql(100);
    });
  });

  context("when the list has multiple elements", () => {
    it("it should return second last element", () => {
      let result = findLastButOneElement([100, 101, 102]);
      expect(result).to.be.eql(101);
    });
  });

  context("when no list is passed", () => {
    it("should return nothing", () => {
      let result = findLastButOneElement();
      expect(result).to.be.undefined;
    });
  });
});

const findLastButOneElement = (list = []) => {
  return list[list.length - 2];
};
