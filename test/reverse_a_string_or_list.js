const chai = require("chai");
const expect = chai.expect;

describe("reverse a string or list", () => {
  context("when a list or string is empty", () => {
    it("should return undefined", () => {
      let result = reverseList([]);
      expect(result).to.be.undefined;
    });
  });

  context("where no list or string is passed", () => {
    it("should return undefined", () => {
      let result = reverseList();
      expect(result).to.be.undefined;
    });
  });

  context("when there are multiple elements in the string or list", () => {
    it("should reverse the string or list and return it", () => {
      let result = reverseList([1,2,3,"banana"]);
      expect(result).to.be.eql(["banana",3,2,1]);
    });
  });
});

const reverseList = (list=[]) => {
  return typeof list === "string"
    ? list.length > 1
      ? list.split("").reverse().join("")
      : undefined
    : list.length > 1
    ? list.reverse()
    : undefined;
};
