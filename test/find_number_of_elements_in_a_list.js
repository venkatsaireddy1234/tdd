const chai = require("chai");
const expect = chai.expect;

describe("find the number of elements in the list or string", () => {
  context("when the list/string is empty", () => {
    it("should return undefined", () => {
      let result = findNoOfElements("");
      expect(result).to.be.undefined;
    });
  });


  context("when there is only one element in the list or string",()=>{
    it("should return the first element in the list or string",()=>{
        let result = findNoOfElements("h")
        expect(result).to.be.eql(1)
    })
  })
  context("when there are mutliple elements in the list/string", () => {
    it("should return length of the list or string", () => {
      let result = findNoOfElements("hello, world!");
      expect(result).to.be.eql(13);
    });
  });
});

const findNoOfElements = (list) => {
  return list.length >= 1 ? list.length : undefined;
};
